# Architecture Diagram

 - Client (Nuxt.js Front end): User enters a request which is sent to the Nuxt.js server.
 - NuxtJS Server: Receives the client's request, processes it and makes appropriate calls to the Google Place API. Once it receives the data from API, it sends it back to the front end.
 - Google Place API: Processes requests from Nuxt JS and sends appropriate data.

![click for preview architecture diagram image](https://gitlab.com/testing2540434/scg-nuxt-3/-/blob/7b270dc2d27446539ec77759c3ebbc668145bc4f/architecture-diagram.png)
# Nuxt 3 Minimal Starter
Look at the [Nuxt 3 documentation](https://nuxt.com/docs/getting-started/introduction) to learn more.

## Evironment Setup
 - Rename the .env.example file to .env.
 - Set your GOOGLE_API_KEY in the .env file (you can get api key from email)
## Setup program

Ensure that all dependencies are installed. If you plan to use Docker, make sure it's installed on your machine.
```bash
#docker 
docker compose up

# npm
npm install

# pnpm
pnpm install

# yarn
yarn install

```
## Development Server

Launch the development server at http://localhost:3000 with the following commands as per the package manager you're using:

```bash
#docker
#you can open http://localhost:3000 on your browser after run docker-compose up 

# npm
npm run dev

# pnpm
pnpm run dev

# yarn
yarn dev
```
