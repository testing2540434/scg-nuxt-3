export default defineEventHandler(async (event) => {
    try {
        const keyword = getQuery(event).keyword;
        const config = useRuntimeConfig();
        
        const res =  await fetch(`https://maps.googleapis.com/maps/api/place/textsearch/json?query=${keyword}&location=${keyword}&radius=1000&type=restaurant&key=${config.public.googleApiKey}`);
        const data = await res.json();

        return data;
    } catch(e) {
        return e;
    }
})